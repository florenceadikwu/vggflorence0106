const ul = document.getElementById('blog-news');
const postUrl = "http://jsonplaceholder.typicode.com/posts"
let containerDiv = document.querySelector('#containerdiv');
let commentsDiv = document.querySelector('#comments-div');
let getForm = document.querySelector('#form');



function getPosts() {
    fetch(postUrl)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            (function () {
                document.getElementById("first").addEventListener("click", firstPage);
                document.getElementById("next").addEventListener("click", nextPage);
                document
                    .getElementById("previous")
                    .addEventListener("click", previousPage);
                document.getElementById("last").addEventListener("click", lastPage);


                var list = data;
                var pageList = [];
                var currentPage = 1;
                var numberPerPage = 10;
                var numberOfPages = 0;
                function makeList() {
                    numberOfPages = getNumberOfPages();
                }

                function getNumberOfPages() {
                    return Math.ceil(list.length / numberPerPage);
                }
                function nextPage() {
                    currentPage += 1;
                    loadList();
                }
                function previousPage() {
                    currentPage -= 1;
                    loadList();
                }
                function firstPage() {
                    currentPage = 1;
                    loadList();
                }
                function lastPage() {
                    currentPage = numberOfPages;
                    loadList();
                }
                function loadList() {
                    var begin = ((currentPage - 1) * numberPerPage);
                    var end = begin + numberPerPage;
                    pageList = list.slice(begin, end);
                    drawList();
                    check();
                }
                function drawList() {
                    ul.innerHTML = "";
                    pageList.forEach(el => {
                        console.log(pageList)
                        ul.innerHTML += `
        <button class="accordion" onClick = "accordionFunc()"><img src="Images/afriwomen.jpg" class="my-portfolio-2">
        <b>${el.title}</b>    <span class="posted"><em>-posted 2 days ago</em></span></button>
        <div class="panel">
          <p>${el.body} ...(continue) </p> 
          <a class="view-more" href='index2.html?id=${el.id}'>Continue</a>
        </div>
        `
                    });
                    // .catch(err => {
                    //     console.warn("something went wrong", err)
                    // });
                }
                function check() {
                    document.getElementById("next").disabled = currentPage == numberOfPages ? true : false;
                    document.getElementById("previous").disabled = currentPage == 1 ? true : false;
                    document.getElementById("first").disabled = currentPage == 1 ? true : false;
                    document.getElementById("last").disabled = currentPage == numberOfPages ? true : false;
                }
                function load() {
                    makeList();
                    loadList();
                }
                window.onload = load();
            })()
        })
}



let querySearch = location.search;
console.log(querySearch)

let sanitizedQS = querySearch.substring(1);

let sanitizedQSArray = sanitizedQS.split("&");

console.log(sanitizedQSArray);

params = {};

sanitizedQSArray.forEach(function (sanitizedQS) {
    paramsKeyValueArray = sanitizedQS.split("=");
    params[paramsKeyValueArray[0]] = paramsKeyValueArray[1];
});
console.log(params);




getPostAndComments(params.id);

function getPostAndComments(id) {
    fetch(`http://jsonplaceholder.typicode.com/posts/${id}`)
        .then(res => res.json())
        .then(data => {
            console.log(data)
            containerDiv.innerHTML += `
          <b>${data.title}</b><br><br>
           <img src="Images/afriwomen.jpg" class="my-portfolio">

           ${data.body}Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laboruLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco<br> <i class="fas fa-thumbs-up ic"></i><i class="fas fa-thumbs-down ico"></i><i class="fas fa-share-alt ico"></i>`


            // .catch(err => {
            //     console.warn("something went wrong", err)
            // });
        })

    fetch(`http://jsonplaceholder.typicode.com/posts/${id}/comments/?postId=${id}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            data.forEach(el => {

                commentsDiv.innerHTML += `
                <div class="margin-bottom">
                <i class="fas fa-user user "></i>
                <p class="name">${el.name}</p>
                <p>${el.body}</p>
                <p class="email">${el.email}</p>
                <p class="icc">&#x1F497   &#x1F600   &#x1F44F &#x1F44E</p>
                </div><hr>
                `
                getForm.innerHTML += `
                
                
                

                
                
                
                
                
                
                `

            });

            // .catch(err => {
            //     console.warn("something went wrong", err)
            // });
        })
}


getPosts();



function accordionFunc() {
    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {

            /* Toggle between adding and removing the "active" class,
            to highlight the button that controls the panel */
            this.classList.toggle("active");

            /* Toggle between hiding and showing the active panel */
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
}

